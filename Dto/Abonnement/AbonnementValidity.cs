namespace Dto.Abonnement;

public enum AbonnementValidityView
{
    OneDay,
    OneMonth,
    ThreeMonths,
    HalfYear,
    Year
}