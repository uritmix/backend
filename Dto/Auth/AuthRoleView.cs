namespace Dto.Auth;

public enum AuthRoleView
{
    Manager,
    Admin,
    Server
}