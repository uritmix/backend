namespace Dto.Auth;

public enum AuthStatusView
{
    NotActivated,
    Activated,
    Blocked
}